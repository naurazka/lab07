$(document).ready(function(){
    $("#theme2").hide();
    $("h3").css("background-color", "#180c62");
    $("h3").css("color", "white");
    $("#accordion").accordion({
        collapsible : true,
        active : false,
        heightStyle: "content",
    });

    $("#theme1").click(function(){
        $("body").css("background-image", "url('static/images/background2.jpg')");
        $("#judul").css('color', '#f7d4d0');
        $("h3").css("background-color", "#fffff");
        $("h3").css("color", "black");
        $("#theme1").hide();
        $("#theme2").show();

    })

    $("#theme2").click(function(){
        $("body").css("background-image", "url('static/images/background1.jpg')");
        $("#judul").css('color', '#180c63');
        $("h3").css("background-color", "#180c62");
        $("h3").css("color", "white");
        $("#theme2").hide();
        $("#theme1").show();

    })
});

