from django.test import TestCase, Client
from django.urls import resolve
from .views import homepage

class HomepageUnitTest(TestCase):
    def test_homepage_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_homepage_using_right_func(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)
# Create your tests here.
